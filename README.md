# modules_and_packages_2022_07_19_gr_0706

---

[video](https://youtu.be/0R1v8NCrzCQ)

---

- поточний каталог
- список каталогів із змінної середовища PYTHONPATH
- залежний від встановлення Python список

> sys.path = ['', '/home/kos/.pyenv/versions/3.9.12/lib/python39.zip', '/home/kos/.pyenv/versions/3.9.12/lib/python3.9', '/home/kos/.pyenv/versions/3.9.12/lib/python3.9/lib-dynload', '/home/kos/.local/lib/python3.9/site-packages', '/home/kos/.pyenv/versions/3.9.12/lib/python3.9/site-packages']

> import re
> print(re.__file__)

> import <modulr_name>

> import <module_name_1>[, <module_name_2, ...>]

> from <module_name> import <name_1>[, <name_2>, ...]

> from <module_name> import *

> from <module_name import <name> as <alt_name>[, <name_2> as <alt_name_2, ...>]

> import <module_name> as <alt_name>

```python
def  bar():
    from mod import foo
    foo("ggggggg")
```


## SyntaxError  
```python
def  bar():
    from mod import *
    foo("ggggggg")
```

> ImportError

> dir()

## Перезавантаження модуля

> import importlib
> importlib.reload(mod)

## Пакети

1) крапкова нотація

> from <pkg> import <module_name_1[, <module_name_2>, ...]>


